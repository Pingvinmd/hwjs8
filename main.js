// DOM це незалежний программний інтерфейс який дає скриптам доступ щоб міняти зіст
//
//innerText витягує та встановлює вміст тега як звичайний текст, тоді як innerHTML витягує та встановлює вміст у форматі HTML.
//
// document.getElementById(),getElementsByName(),getElementsByTagName(),getElementsByClassName(),document.querySelector(),
//document.querySelectorAll(). Кращій спосіб querySelector().


const elems = document.querySelectorAll('p');
elems.forEach((e) => {
  e.style.cssText = "background: #ff0000";
});
console.log(elems);

const elem = document.getElementById("optionsList");
console.log(elem);

const parentElement = elem.parentElement;
console.log(parentElement);

const childNodes = elem.childNodes;
console.log(childNodes);

const elemTestParagraph = document.getElementById("testParagraph");
elemTestParagraph.textContent = "This is a paragraph";
console.log(elemTestParagraph);

const elemMainHeader = document.querySelector(".main-header");
const getAllChildren = (element) => {
  if (element.children.length === 0) return [element];
  let allChildElements = [];
  for (let i = 0; i < element.children.length; i++) {
    let children = getAllChildren(element.children[i]);
    if (children) allChildElements.push(...children);
  }
  allChildElements.push(element);
  return allChildElements;
};
const allChildren = getAllChildren(elemMainHeader)
for(let i = 0; i < allChildren.length; i++) {
  allChildren[i].className = 'nav-item';
  }
console.log(getAllChildren(elemMainHeader));


const elemsClassName = document.querySelectorAll('.section-title');
elemsClassName.forEach((e) => {
  e.className.remove = ("section-title");
});
console.log(elemsClassName);